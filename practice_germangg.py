# TODO
        # sentences examples included
        # sentences translated with deepl.com 
        # case-sensitivness to be tested 
        # program shall validate the data before passing it to the parser 
            # https://german.stackexchange.com/questions/491/where-can-i-find-a-parsable-list-of-german-words
        # 
        # try catch if there is not internet connection 
        # .
        # .
        # .
        # partizip I, partizip II, infinitiv mit zu - someday in the future, once I know what these are ;] 

        # ++++ text formatting shall be tweaked (so the columns are fixed size) ++++ DONE 
        # ++++ now make it work also for verbs ++++ DONE

import requests
from bs4 import BeautifulSoup
import sys 

def print_neatly(grammatik_table, is_it_noun, is_it_verb):
    # grammatik_table is a list ; 8 elements list 
    # retstr = "          |  Singular            |   Plural   \n"
    # retstr += "----------|----------------------|-----------\n"
    # retstr += "NOMINATIV |  {}                  |   {}\n".format(grammatik_table[0], grammatik_table[1])
    # retstr += "----------|----------------------|-----------\n"
    # retstr += "AKKUSATIV |  {}                  |   {}\n".format(grammatik_table[6], grammatik_table[7])
    # retstr += "----------|----------------------|-----------\n"
    # retstr += "DATIV     |  {}                  |   {}\n".format(grammatik_table[4], grammatik_table[5])
    # retstr += "----------|----------------------|-----------\n"
    # retstr += "GENITIV   |  {}                  |   {}\n".format(grammatik_table[2], grammatik_table[3])

    if (is_it_noun):
        retstr = "{:10s}|{:48s}|{:24s}\n".format("", "  Singular  ", "  Plural")
        retstr += "{:10s}|{:48s}|{:24s}\n".format("----------", "------------------------------------------------", "------------------------")
        retstr += "{:10s}|  {:46s}|  {:24s}\n".format("NOMINATIV", grammatik_table[0], grammatik_table[1])
        retstr += "{:10s}|{:48s}|{:24s}\n".format("----------", "------------------------------------------------", "------------------------")
        retstr += "{:10s}|  {:46s}|  {:24s}\n".format("AKKUSATIV", grammatik_table[6], grammatik_table[7])
        retstr += "{:10s}|{:48s}|{:24s}\n".format("----------", "------------------------------------------------", "------------------------")
        retstr += "{:10s}|  {:46s}|  {:24s}\n".format("DATIV", grammatik_table[4], grammatik_table[5])
        retstr += "{:10s}|{:48s}|{:24s}\n".format("----------", "------------------------------------------------", "------------------------")
        retstr += "{:10s}|  {:46s}|  {:24s}\n".format("GENITIV", grammatik_table[2], grammatik_table[3])

        #           8spacessss|__{}
        #           8characte_|__{48 chars}__|__{24chars}\n == 8 space | spacespace 48 spacespace | spacespace 24\n

        return retstr

    ''' 

              |  Singular            |   Plural   
    ----------|----------------------|------------
    NOMINATIV |  der Hund            |   die Hunde
    ----------|----------------------|------------
    AKKUSATIV |  den Hund            |   die Hunde
    ----------|----------------------|------------
    DATIV     |  dem Hund            |   den Hunden
    ----------|----------------------|------------
    GENITIV   |  des Hundes, Hunds   |   der Hunde

    '''

        # [singularNominativ, pluralNominativ
        #  singular Genitiv,  pluralGenitiv
        #  singularDativ,     pluralDativ
        #  singularAkkusativ, pluralAkkusativ]

    if (is_it_verb):
        retstr = "{:10s}|  {:34s}|  {:34s}|  {:34s}\n".format("", "INDIKATIV", "KONJUKTIV I", "IMPERATIV")
        retstr += "{:10s}|{:34s}|{:34s}|{:34s}\n".format("----------", "------------------------------------", "------------------------------------", "------------------------------------")
        retstr += "{:10s}|  {:34s}|  {:34s}|  {:34s}\n".format("S", grammatik_table[0], grammatik_table[1], grammatik_table[2])
        retstr += "{:10s}|  {:34s}|  {:34s}|  {:34s}\n".format("I", grammatik_table[3], grammatik_table[4], grammatik_table[5])
        retstr += "{:10s}|  {:34s}|  {:34s}|  {:34s}\n".format("N.", grammatik_table[6], grammatik_table[7], grammatik_table[8])
        retstr += "{:10s}|{:34s}|{:34s}|{:34s}\n".format("----------", "------------------------------------", "------------------------------------", "------------------------------------")
        retstr += "{:10s}|  {:34s}|  {:34s}|  {:34s}\n".format("P", grammatik_table[9], grammatik_table[10], grammatik_table[11])
        retstr += "{:10s}|  {:34s}|  {:34s}|  {:34s}\n".format("L", grammatik_table[12], grammatik_table[13], grammatik_table[14])
        retstr += "{:10s}|  {:34s}|  {:34s}|  {:34s}\n".format("U.", grammatik_table[15], grammatik_table[16], grammatik_table[17])
        retstr += "\nund doch gibt es Praeteritum (Past simple)\n\n"
        retstr += "{:10s}|  {:34s}|  {:34s}|\n".format("", "INDIKATIV", "KONJUKTIV II")
        retstr += "{:10s}|{:34s}|{:34s}|\n".format("----------", "------------------------------------", "------------------------------------", "------------------------------------") # format function is elastic! 
        retstr += "{:10s}|  {:34s}|  {:34s}|\n".format("S", grammatik_table[18], grammatik_table[19])
        retstr += "{:10s}|  {:34s}|  {:34s}|\n".format("I", grammatik_table[20], grammatik_table[21])
        retstr += "{:10s}|  {:34s}|  {:34s}|\n".format("N.", grammatik_table[22], grammatik_table[23])
        retstr += "{:10s}|{:34s}|{:34s}|\n".format("----------", "------------------------------------", "------------------------------------", "------------------------------------") # it doesn't care that I am passing more arguments than I declared it 
        retstr += "{:10s}|  {:34s}|  {:34s}|\n".format("P", grammatik_table[24], grammatik_table[25])
        retstr += "{:10s}|  {:34s}|  {:34s}|\n".format("L", grammatik_table[26], grammatik_table[27])
        retstr += "{:10s}|  {:34s}|  {:34s}|\n".format("U.", grammatik_table[28], grammatik_table[29])
        
        return retstr 
'''
          |  INDIKATIV               |  KONJUKTIV I             |  IMPERATIV [left margin the same in all columns - 2 spaces]
----------|--------------------------|--------------------------|---------------------------------
S         |  ich programmiere        |  ich programmiere        |  -
I         |  du programmierst        |  du programmierest       |  programmier, programmiere! 
N.        |  er/sie/es programmiert  |  er/sie/es programmiere  |  -                           # len("er/sie/es programmiert") is 22 ; let's set this col size to 34 (30 + 2 + 2 margins) 
----------|--------------------------|--------------------------|---------------------------------
P         |  wir programmieren       |  wir programmieren       |  -
L         |  ihr programmiert        |  ihr programmieret       |  programmiert! 
U.        |  sie/Sie programmieren   |  sie/Sie programmieren   |  - 

"und doch gibt es Praeteritum (Past simple)"

          |  INDIKATIV              |  KONJUKTIV II           |
----------|---------------------------|---------------------------|
S         |  ich programmierte        |  ich programmierte        |
I         |  du programmiertest       |  du programmiertest       |
N.        |  er/sie/es programmierte  |  er/sie/es programmierte  |
----------|-------------------------- |---------------------------|
P         |  wir programmierten       |  wir programmierten       |
L         |  ihr programmiertet       |  ihr programmiertet       |
U.        |  sie/Sie programmierten   |  sie/Sie programmierten   |

'''

# ask for input
word = input("What is to be translated? [in german]\n")

# prepare address for parser 
webaddress = "https://www.duden.de/suchen/dudenonline/{}".format(word)

# load webpage 
r2 = requests.get(webaddress, headers={'User-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'})
# r = requests.get("http://www.pyclass.com/example.html", headers={'User-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'})
# r2 = requests.get("https://www.duden.de/suchen/dudenonline/suchen", headers={'User-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'})


# load webpage contents
contents2 = r2.content
#contents = r.content


# print(type(contents2)) # class 'bytes' ; so this is definitely not a string! 
# print(contents2)

# here is where the BeautifulSoup comes into play!
soup = BeautifulSoup(contents2, "html.parser")

# write this print to the file:
# f = open("prettyfied.txt", 'w')
# print((soup.prettify()).encode('utf-8'), file = f) # encoding('utf-8') did the job 
# see the solution here: https://stackoverflow.com/questions/14630288/unicodeencodeerror-charmap-codec-cant-encode-character-maps-to-undefined 

# no need to print this, but it looks amazing! I wonder how it's done :) 
# print(soup.prettify())

pre_find = soup.find("p", {"class": "vignette__snippet"})

# for each_word in pre_find.text: # this works on CHARS not on strings Szymi ;] 
#     print(each_word,)
#     if (each_word == "Verb"):
#         print("This is a verb")
#         break

verb = False
noun = False

if ("Verb" in pre_find.text) or ("verb" in pre_find.text):
    verb = True
    # print("This is a verb")
    webaddress = "https://www.duden.de/rechtschreibung/{}".format(word)
    r2 = requests.get(webaddress, headers={'User-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'})
    contents2 = r2.content
    soup = BeautifulSoup(contents2, "html.parser")
    pre_find = soup.find_all("td")
    # print(pre_find.text)
    ldummy_ret = []
    for item in pre_find:
        #print(item.find_all("td")[0].text)
        # print(item.text)
        ldummy_ret.append(item.text)
        if len(ldummy_ret) == 30: # this is nr of <td>'s including data we need 
            break

    # print(print_neatly(ldummy_ret, noun, verb)) # TODO this can be optimized I guess, why 3 arguments when there can be only 2? 
    # print(ldummy_ret)

    #### RESTART HERE, USING JUPYTER NOTEBOOK #### 
    # NOW IT'S TIME TO ADD EXAMPLE SENTENCES 
    # <dt class="note__title">Beispiele</dt>
    pre_find = 0
    pre_find = soup.find("dt", {"class": "Beispiele"})
    if (not pre_find):
        pre_find = soup.find("dt", {"class": "Beispiel"})
    pre_find = soup.find_all("li")
    ldummy_ret = []
    for item in pre_find:
        ldummy_ret.append(item.text)

    print(ldummy_ret)
    
elif ("Substantiv" in pre_find.text) or ("substantiv" in pre_find.text):
    noun = True 
    #### print("This is a noun!") ####
    webaddress = "https://www.duden.de/rechtschreibung/{}".format(word)
    r2 = requests.get(webaddress, headers={'User-agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0'})
    contents2 = r2.content
    soup = BeautifulSoup(contents2, "html.parser")
    pre_find = soup.find_all("td")
    #print(pre_find[0].text)
    #print(pre_find)
    #ldummy_ret = ["", "", "", "", "", "", "", ""]
    ldummy_ret = []
    for item in pre_find:
        #print(item.find_all("td")[0].text)
        # print(item.text)
        ldummy_ret.append(item.text)
        if len(ldummy_ret) == 8:
            break
        # [singularNominativ, pluralNominativ
        #  singular Genitiv,  pluralGenitiv
        #  singularDativ,     pluralDativ
        #  singularAkkusativ, pluralAkkusativ] 

    #print(ldummy_ret)
    print(print_neatly(ldummy_ret, noun, verb))

# TODO
    # check in PEP8 style guide if brackets () shall be separated with text with space or not [if () or if()]
    # check in PEP8 style guide if == and = shall be separated with thext with space or not [a = 5 or a=5]